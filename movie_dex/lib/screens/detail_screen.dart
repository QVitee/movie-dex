import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:moviedex/bloc/get_movie_videos_bloc.dart';
import 'package:moviedex/model/movie.dart';
import 'package:moviedex/model/video.dart';
import 'package:moviedex/model/video_response.dart';
import 'package:moviedex/style/theme.dart' as Style;
import 'package:moviedex/widgets/casts.dart';
import 'package:moviedex/widgets/movie_info.dart';
import 'package:moviedex/widgets/similar_movies.dart';
import 'package:sliver_fab/sliver_fab.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'video_player.dart';

class MovieDetailScreen extends StatefulWidget {
  final Movie movie;
  MovieDetailScreen({Key key, @required this.movie}) : super(key: key);
  @override
  _MovieDetailScreenState createState() => _MovieDetailScreenState(movie);
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {
  final Movie movie;
  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  _MovieDetailScreenState(this.movie);

  @override
  void initState() {
    super.initState();
    movieVideosBloc..getMovieVideos(movie.id);
  }

//   @override
//   void dispose() {
//     super.dispose();
//     movieVideosBloc..drainStream();
//   }

  Future<void> _refresh() async {
    movieVideosBloc..getMovieVideos(movie.id);
  }

  @override
  Widget build(BuildContext context) {
    print("build detail");
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: new Builder(
          builder: (context) {
            return new SliverFab(
              floatingPosition: FloatingPosition(right: 20),
              floatingWidget: StreamBuilder<VideoResponse>(
                stream: movieVideosBloc.subject.stream,
                builder: (context, AsyncSnapshot<VideoResponse> snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.error != null &&
                        snapshot.data.error.length > 0) {
                      return _buildErrorWidget(snapshot.data.error);
                    }
                    return _buildVideoWidget(snapshot.data);
                  } else if (snapshot.hasError) {
                    return _buildErrorWidget(snapshot.error);
                  } else {
                    return _buildLoadingWidget();
                  }
                },
              ),
              expandedHeight: 235.0,
              slivers: <Widget>[
                new SliverAppBar(
                  backgroundColor: Style.Colors.mainColor,
                  expandedHeight: 220.0,
                  pinned: true,
                  flexibleSpace: new FlexibleSpaceBar(
                      centerTitle: true,
                      title: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 3, 5, 0),
                        child: Text(
                          // movie.title,
                          movie.title.length > 40
                              ? movie.title.substring(0, 37) + "..."
                              : movie.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 17.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                      background: Stack(
                        children: <Widget>[
                          Container(
                            height: 220,
                            alignment: Alignment.bottomCenter,
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.black.withOpacity(0.3),
                            ),
                            child: CachedNetworkImage(
                              imageUrl: "https://image.tmdb.org/t/p/original/" +
                                  movie.backPoster.toString(),
                              fit: BoxFit.fill,
                              errorWidget: (context, url, error) => Container(
                                color: Colors.black26,
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.bottomCenter,
                                  end: Alignment.topCenter,
                                  stops: [
                                    0.1,
                                    0.9
                                  ],
                                  colors: [
                                    Colors.black.withOpacity(0.9),
                                    Colors.black.withOpacity(0.0)
                                  ]),
                            ),
                          ),
                        ],
                      )),
                ),
                SliverPadding(
                    padding: EdgeInsets.all(0.0),
                    sliver: SliverList(
                        delegate: SliverChildListDelegate([
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 20.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              movie.rating.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            RatingBar(
                              itemSize: 12.0,
                              initialRating: movie.rating / 2,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context, _) => Icon(
                                EvaIcons.star,
                                color: Style.Colors.secondColor,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 20.0),
                        child: Text(
                          "OVEWVIEW",
                          style: TextStyle(
                              color: Style.Colors.titleColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12.0),
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          movie.overview != "" ? movie.overview : "Unkonwn",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.white, fontSize: 15.0, height: 1.5),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      MovieInfo(
                        id: movie.id,
                      ),
                      Casts(
                        id: movie.id,
                      ),
                      SimilarMovies(id: movie.id)
                    ])))
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [],
    ));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error"),
      ],
    ));
  }

  Widget _buildVideoWidget(VideoResponse data) {
    List<Video> videos = data.videos;
    return FloatingActionButton(
      backgroundColor: Style.Colors.secondColor,
      onPressed: () {
        if (videos.length != 0) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => VideoPlayerScreen(
                controller: YoutubePlayerController(
                  initialVideoId: videos[0].key.toString(),
                  flags: YoutubePlayerFlags(
                    autoPlay: true,
                    mute: false,
                  ),
                ),
              ),
            ),
          );
        } else {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                elevation: 1000,
                backgroundColor: Style.Colors.mainColor,
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Style.Colors.secondColor,
                    width: 1
                  ),
                    borderRadius: BorderRadius.circular(20)),
                title: Text(
                  "Notice",
                  style: TextStyle(color: Style.Colors.secondColor),
                ),
                content: Text(
                  "This film has no Trailer video.",
                  style: TextStyle(color: Colors.white),
                ),
              );
            },
          );
        }
      },
      child: Icon(Icons.play_arrow),
    );
  }
}
