class PersonCreditMovie {
  final int id;
  final String character;
  final String title;
  final String originalTitle;
  final String poster;
  final String overview;
  final double rating;

  PersonCreditMovie(this.id,
      this.character,
      this.title,
      this.originalTitle,
      this.poster,
      this.overview,
      this.rating);

  PersonCreditMovie.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        character = json["character"],
        title = json["title"],
        originalTitle = json["original_title"],
        poster = json["poster_path"],
        overview = json["overview"],
        rating = json["vote_average"].toDouble();
}
