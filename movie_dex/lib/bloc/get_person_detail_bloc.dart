      import 'package:flutter/material.dart';
import 'package:moviedex/model/person_detail_response.dart';
import 'package:moviedex/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class PersonDetailBloc {
  final MovieRepository _repository = MovieRepository();
  BehaviorSubject<PersonDetailResponse> _subject =
      BehaviorSubject<PersonDetailResponse>();

  getPersonDetail(int id) async {
    PersonDetailResponse response = await _repository.getPersonsDetail(id);
    _subject.sink.add(response);
  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<PersonDetailResponse> get subject => _subject;
}
final personDetailBloc = PersonDetailBloc();