import 'package:flutter/material.dart';
import 'package:moviedex/model/movie_response.dart';
import 'package:moviedex/model/person_movie_credit.dart';
import 'package:moviedex/model/person_movie_credit_response.dart';
import 'package:moviedex/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class CreditMoviesBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<PersonCreditMovieResponse> _subject = BehaviorSubject<PersonCreditMovieResponse>();

  getCreditMovies(int id) async {
    PersonCreditMovieResponse response = await _repository.getPersonsMovieCredits(id);
    _subject.sink.add(response);
  }

  void drainStream(){_subject.value = null;}
  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<PersonCreditMovieResponse> get subject => _subject;
}
final creditMoviesBloc = CreditMoviesBloc();